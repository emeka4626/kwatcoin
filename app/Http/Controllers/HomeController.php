<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Yabacon\Paystack;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function join()
    {
        return view('auth.register');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email', 'unique:waiting'],
            'name' => ['required'],
            'phone' => ['required'],
            'amount' => ['required']
        ]);

        $paymentUrl = $this->makePayment($request->input('amount'), $request->input('email'));

        DB::table('waiting')->insert([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'amount' => $request->input('amount'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        session()->flash('success', 'You have successfully joined the waiting list');
        return redirect($paymentUrl);
    }

    private function makePayment($amount, $email)
    {
        $total = (540 * $amount) * 100;

        $paystack = new Paystack('sk_live_2ef9e5bf1b9ac933d902aa9b1a172d6dafdba3b7');

        $transaction = $paystack->transaction->initialize([
            'amount' => $total,
            'email' => $email,
            'reference' => Str::random()
        ]);

        return $transaction->data->authorization_url;

    }
}
