<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kwatcoin</title>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{ asset('app/vendor/waves/waves.min.css') }}">
    <link rel="stylesheet" href="{{ asset('app/vendor/owlcarousel/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('app/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.css">
</head>
<body>
    <div id="app">
        <div id="main-wrapper">

            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <nav class="navbar navbar-expand-lg navbar-light px-0 justify-content-between">
                                <a class="navbar-brand" href="/">
{{--                                    <img src="images/w_logo.png" alt="">--}}
                                    <span>KwatCoin</span></a>

{{--                                <div class="dashboard_log">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <div class="header_auth">--}}
{{--                                            <a href="signin.html" class="btn btn-success  mx-2">Sign In</a>--}}
{{--                                            <a href="signup.html" class="btn btn-outline-primary  mx-2">Sign Up</a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>


            @yield('content')

{{--            <div class="footer">--}}
{{--                <div class="container">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-xl-6 col-md-6">--}}
{{--                            <div class="footer-link text-left">--}}
{{--                                <a href="#" class="m_logo"><img src="images/w_logo.png" alt=""></a>--}}
{{--                                <a href="about.html">About</a>--}}
{{--                                <a href="privacy-policy.html">Privacy Policy</a>--}}
{{--                                <a href="term-condition.html">Term & Service</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-6 col-md-6 text-lg-right text-center">--}}
{{--                            <div class="social">--}}
{{--                                <a href="#"><i class="fa fa-youtube-play"></i></a>--}}
{{--                                <a href="#"><i class="fa fa-instagram"></i></a>--}}
{{--                                <a href="#"><i class="fa fa-twitter"></i></a>--}}
{{--                                <a href="#"><i class="fa fa-facebook"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row align-items-center">--}}
{{--                        <div class="col-xl-12 text-center text-lg-right">--}}
{{--                            <div class="copy_right text-center text-lg-center">--}}
{{--                                Copyright © 2019 Quixlab. All Rights Reserved.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <div class="bg_icons"></div>
    </div>

    <script src="{{ asset('app/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('app/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('app/vendor/waves/waves.min.js') }}"></script>

    <script src="{{ asset('app/vendor/validator/jquery.validate.js') }}"></script>
    <script src="{{ asset('app/vendor/validator/validator-init.js') }}"></script>
    <script src="{{ asset('app/js/scripts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
    <script>
        @if(\Illuminate\Support\Facades\Session::has('success'))
        new Noty({
            type: 'success',
            layout: 'topRight',
            text: "{{ \Illuminate\Support\Facades\Session::get('success') }}",
            theme: 'semanticui',
            timeout: 10000
        }).show();
        @elseif(\Illuminate\Support\Facades\Session::has('error'))
        new Noty({
            type: 'error',
            layout: 'topRight',
            text: "{{ \Illuminate\Support\Facades\Session::get('error') }}",
            theme: 'semanticui',
            timeout: 10000
        }).show();
        @elseif(\Illuminate\Support\Facades\Session::has('alert'))
        new Noty({
            type: 'alert',
            layout: 'topRight',
            text: "{{ \Illuminate\Support\Facades\Session::get('alert') }}",
            theme: 'semanticui',
            timeout: 10000
        }).show();
        @endif
    </script>

</body>
</html>
