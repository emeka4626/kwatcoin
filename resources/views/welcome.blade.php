<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Kwatcoin | Welcome</title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="The Kwatcoin cryptocurrency platform">
    <link rel="icon" href="favicon.ico">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('default/assets/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('default/assets/css/default.css') }}"/>
    <link rel="stylesheet" href="{{ asset('default/assets/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('default/assets/css/font-awesome/css/font-awesome.css') }}"/>
    <link rel="stylesheet" href="{{ asset('default/assets/css/slick.css') }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.css">
    <!--fonts css-->
    <link
        href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
</head>

<body>
<section>
    <!-- Navigation Start -->
    <nav class="navbar navbar-expand-lg navbar-light sticky-top" id="nav-main">
        <div class="container">
            <a href="/" class="navbar-bran">
                <b style="font-size: 20px; color: #B49A5B">KwatCoin</b>
            </a>
            <button
                class="navbar-toggler"
                data-target="#navigation"
                data-control="navigation"
                data-toggle="collapse"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav pull-lg-right ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#feature">Features</a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link page-scroll" href="#coin">Coin Sale</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link page-scroll" href="#roadmap">RoadMap</a>--}}
{{--                    </li>--}}
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#faq">FAQ</a>
                    </li>
                </ul>
                <ul class="ml-auto">
                    <li class="nav-item">
                        <a class="nav-link btn-contact page-scroll" href="{{ url('/join') }}">Join Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--Navigation End -->
    <!-- Hero Section Start -->
    <section id="platform">
        <div class="container">
            <div class="landing-home">
                <h1 class="text-center">
                    Kwatcoin is the new kind of money
                    {{--                    <span>crypto intelligence</span>--}}
                </h1>
                <div class="landing-svg">
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="195.356mm" height="137.442mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                         viewBox="0 0 19343 13609"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
 <defs>
     <style type="text/css">
         <![CDATA[
         .str1 {stroke:white;stroke-width:38.5656;stroke-opacity:0.639216}
         .str0 {stroke:white;stroke-width:38.5656;stroke-opacity:0.800000}
         .str2 {stroke:#4B4BD3;stroke-width:38.5656;stroke-dasharray:269.929357 2429.364217}
         .fil56 {fill:none}
         .fil63 {fill:none}
         .fil62 {fill:none}
         .fil84 {fill:#111231}
         .fil87 {fill:#131433}
         .fil83 {fill:#161535}
         .fil86 {fill:#161733}
         .fil57 {fill:#373D61}
         .fil88 {fill:#43455E}
         .fil54 {fill:#4B4BD3}
         .fil85 {fill:#7F7FBB}
         .fil101 {fill:#C895FD}
         .fil127 {fill:#C895FD}
         .fil82 {fill:#DD966A}
         .fil58 {fill:#E5BD95}
         .fil81 {fill:#F0B387}
         .fil55 {fill:#F79294}
         .fil59 {fill:#F9D3E3}
         .fil52 {fill:#FFD8AD}
         .fil53 {fill:white}
         .fil67 {fill:black;fill-opacity:0.121569}
         .fil38 {fill:black;fill-opacity:0.290196}
         .fil120 {fill:url(#id0)}
         .fil110 {fill:url(#id1)}
         .fil107 {fill:url(#id2)}
         .fil112 {fill:url(#id3)}
         .fil111 {fill:url(#id4)}
         .fil117 {fill:url(#id5)}
         .fil126 {fill:url(#id6)}
         .fil115 {fill:url(#id7)}
         .fil119 {fill:url(#id8)}
         .fil125 {fill:url(#id9)}
         .fil123 {fill:url(#id10)}
         .fil114 {fill:url(#id11)}
         .fil113 {fill:url(#id12)}
         .fil124 {fill:url(#id13)}
         .fil118 {fill:url(#id14)}
         .fil116 {fill:url(#id15)}
         .fil121 {fill:url(#id16)}
         .fil122 {fill:url(#id17)}
         .fil73 {fill:url(#id18)}
         .fil105 {fill:url(#id19)}
         .fil92 {fill:url(#id20)}
         .fil41 {fill:url(#id21)}
         .fil1 {fill:url(#id22)}
         .fil6 {fill:url(#id23)}
         .fil11 {fill:url(#id24)}
         .fil64 {fill:url(#id25)}
         .fil3 {fill:url(#id26)}
         .fil8 {fill:url(#id27)}
         .fil13 {fill:url(#id28)}
         .fil37 {fill:url(#id29)}
         .fil50 {fill:url(#id30)}
         .fil99 {fill:url(#id31)}
         .fil91 {fill:url(#id32)}
         .fil106 {fill:url(#id33)}
         .fil74 {fill:url(#id34)}
         .fil97 {fill:url(#id35)}
         .fil104 {fill:url(#id36)}
         .fil94 {fill:url(#id37)}
         .fil46 {fill:url(#id38)}
         .fil89 {fill:url(#id39)}
         .fil65 {fill:url(#id40)}
         .fil100 {fill:url(#id41)}
         .fil90 {fill:url(#id42)}
         .fil93 {fill:url(#id43)}
         .fil109 {fill:url(#id44)}
         .fil76 {fill:url(#id45)}
         .fil80 {fill:url(#id46)}
         .fil66 {fill:url(#id47)}
         .fil0 {fill:url(#id48)}
         .fil5 {fill:url(#id49)}
         .fil10 {fill:url(#id50)}
         .fil79 {fill:url(#id51)}
         .fil9 {fill:url(#id52)}
         .fil4 {fill:url(#id53)}
         .fil78 {fill:url(#id54)}
         .fil69 {fill:url(#id55)}
         .fil108 {fill:url(#id56)}
         .fil75 {fill:url(#id57)}
         .fil72 {fill:url(#id58)}
         .fil30 {fill:url(#id59)}
         .fil28 {fill:url(#id60)}
         .fil44 {fill:url(#id61)}
         .fil49 {fill:url(#id62)}
         .fil42 {fill:url(#id63)}
         .fil33 {fill:url(#id64)}
         .fil35 {fill:url(#id65)}
         .fil32 {fill:url(#id66)}
         .fil34 {fill:url(#id67)}
         .fil95 {fill:url(#id68)}
         .fil29 {fill:url(#id69)}
         .fil31 {fill:url(#id70)}
         .fil48 {fill:url(#id71)}
         .fil17 {fill:url(#id72)}
         .fil27 {fill:url(#id73)}
         .fil22 {fill:url(#id74)}
         .fil16 {fill:url(#id75)}
         .fil26 {fill:url(#id76)}
         .fil12 {fill:url(#id77)}
         .fil21 {fill:url(#id78)}
         .fil2 {fill:url(#id79)}
         .fil7 {fill:url(#id80)}
         .fil103 {fill:url(#id81)}
         .fil96 {fill:url(#id82)}
         .fil45 {fill:url(#id83)}
         .fil36 {fill:url(#id84)}
         .fil77 {fill:url(#id85)}
         .fil71 {fill:url(#id86)}
         .fil47 {fill:url(#id87)}
         .fil98 {fill:url(#id88)}
         .fil43 {fill:url(#id89)}
         .fil19 {fill:url(#id90)}
         .fil24 {fill:url(#id91)}
         .fil14 {fill:url(#id92)}
         .fil23 {fill:url(#id93)}
         .fil18 {fill:url(#id94)}
         .fil40 {fill:url(#id95)}
         .fil15 {fill:url(#id96)}
         .fil20 {fill:url(#id97)}
         .fil25 {fill:url(#id98)}
         .fil51 {fill:url(#id99)}
         .fil70 {fill:url(#id100)}
         .fil102 {fill:url(#id101)}
         .fil68 {fill:url(#id102)}
         .fil39 {fill:url(#id103);fill-opacity:0.160784}
         .fil60 {fill:url(#id104);fill-opacity:0.701961}
         .fil61 {fill:url(#id105);fill-opacity:0.701961}
         ]]>
     </style>
     <linearGradient id="id0" gradientUnits="userSpaceOnUse" x1="11185" y1="6272" x2="10939" y2="6675">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="0.368627" style="stop-opacity:1; stop-color:#8970E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id1" gradientUnits="userSpaceOnUse" x1="6323" y1="9864" x2="6984" y2="9922">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="0.4" style="stop-opacity:1; stop-color:#8970E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id2" gradientUnits="userSpaceOnUse" xlink:href="#id1" x1="4947" y1="9073" x2="5608" y2="9131">
     </linearGradient>
     <linearGradient id="id3" gradientUnits="userSpaceOnUse" xlink:href="#id1" x1="13697" y1="6508" x2="14594" y2="6587">
     </linearGradient>
     <linearGradient id="id4" gradientUnits="userSpaceOnUse" xlink:href="#id1" x1="9276" y1="10012" x2="10173" y2="10091">
     </linearGradient>
     <linearGradient id="id5" gradientUnits="userSpaceOnUse" x1="10399" y1="6050" x2="10506" y2="5995">
         <stop offset="0" style="stop-opacity:1; stop-color:#C895FD"/>
         <stop offset="0.309804" style="stop-opacity:1; stop-color:#8A70E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#4B4BD3"/>
     </linearGradient>
     <linearGradient id="id6" gradientUnits="userSpaceOnUse" x1="10294" y1="5006" x2="10294" y2="5705">
         <stop offset="0" style="stop-opacity:1; stop-color:#C895FD"/>
         <stop offset="0.388235" style="stop-opacity:1; stop-color:#8A70E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#4B4BD3"/>
     </linearGradient>
     <linearGradient id="id7" gradientUnits="userSpaceOnUse" x1="10801" y1="5660" x2="11048" y2="5992">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="0.74902" style="stop-opacity:1; stop-color:#8970E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id8" gradientUnits="userSpaceOnUse" x1="11169" y1="6491" x2="11012" y2="7374">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="0.701961" style="stop-opacity:1; stop-color:#8970E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id9" gradientUnits="userSpaceOnUse" x1="10051" y1="4649" x2="10159" y2="5053">
         <stop offset="0" style="stop-opacity:1; stop-color:#C895FD"/>
         <stop offset="0.588235" style="stop-opacity:1; stop-color:#8A70E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#4B4BD3"/>
     </linearGradient>
     <linearGradient id="id10" gradientUnits="userSpaceOnUse" x1="9428" y1="5708" x2="9834" y2="5418">
         <stop offset="0" style="stop-opacity:1; stop-color:#C895FD"/>
         <stop offset="0.258824" style="stop-opacity:1; stop-color:#8A70E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#4B4BD3"/>
     </linearGradient>
     <linearGradient id="id11" gradientUnits="userSpaceOnUse" x1="11074" y1="5944" x2="11231" y2="5278">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="0.690196" style="stop-opacity:1; stop-color:#8970E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id12" gradientUnits="userSpaceOnUse" x1="10995" y1="5080" x2="11206" y2="5609">
         <stop offset="0" style="stop-opacity:1; stop-color:#C895FD"/>
         <stop offset="0.458824" style="stop-opacity:1; stop-color:#8A70E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#4B4BD3"/>
     </linearGradient>
     <linearGradient id="id13" gradientUnits="userSpaceOnUse" x1="9528" y1="5271" x2="9719" y2="5075">
         <stop offset="0" style="stop-opacity:1; stop-color:#C895FD"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#4B4BD3"/>
     </linearGradient>
     <linearGradient id="id14" gradientUnits="userSpaceOnUse" xlink:href="#id5" x1="10670" y1="6351" x2="10855" y2="6082">
     </linearGradient>
     <linearGradient id="id15" gradientUnits="userSpaceOnUse" x1="10481" y1="6520" x2="10035" y2="6867">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id16" gradientUnits="userSpaceOnUse" x1="10459" y1="5981" x2="10544" y2="6179">
         <stop offset="0" style="stop-opacity:1; stop-color:#4B4BD3"/>
         <stop offset="0.65098" style="stop-opacity:1; stop-color:#8970E8"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#C895FD"/>
     </linearGradient>
     <linearGradient id="id17" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="11235" y1="6011" x2="11020" y2="6264">
     </linearGradient>
     <linearGradient id="id18" gradientUnits="userSpaceOnUse" x1="4577" y1="3676" x2="5888" y2="3676">
         <stop offset="0" style="stop-opacity:1; stop-color:#96CADF"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#DFEDF6"/>
     </linearGradient>
     <linearGradient id="id19" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="4577" y1="4028" x2="5888" y2="4028">
     </linearGradient>
     <linearGradient id="id20" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="1689" y1="4799" x2="2958" y2="4799">
     </linearGradient>
     <linearGradient id="id21" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="6775" y1="8819" x2="6737" y2="10670">
     </linearGradient>
     <linearGradient id="id22" gradientUnits="userSpaceOnUse" x1="15353" y1="2074" x2="17171" y2="2074">
         <stop offset="0" style="stop-opacity:1; stop-color:#DFEDF6"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#96CADF"/>
     </linearGradient>
     <linearGradient id="id23" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="15353" y1="2775" x2="17171" y2="2775">
     </linearGradient>
     <linearGradient id="id24" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="15353" y1="3438" x2="17171" y2="3438">
     </linearGradient>
     <linearGradient id="id25" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="760" y1="5172" x2="5888" y2="5172">
     </linearGradient>
     <linearGradient id="id26" gradientUnits="userSpaceOnUse" x1="15992" y1="2091" x2="16532" y2="2091">
         <stop offset="0" style="stop-opacity:1; stop-color:#CB7300"/>
         <stop offset="1" style="stop-opacity:1; stop-color:#FFC260"/>
     </linearGradient>
     <linearGradient id="id27" gradientUnits="userSpaceOnUse" xlink:href="#id26" x1="15992" y1="2741" x2="16532" y2="2741">
     </linearGradient>
     <linearGradient id="id28" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="15992" y1="3427" x2="16532" y2="3427">
     </linearGradient>
     <linearGradient id="id29" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="9012" y1="4953" x2="11788" y2="4953">
     </linearGradient>
     <linearGradient id="id30" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13484" y1="7466" x2="13484" y2="8682">
     </linearGradient>
     <linearGradient id="id31" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="2726" y1="7508" x2="3690" y2="7508">
     </linearGradient>
     <linearGradient id="id32" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="1689" y1="5426" x2="2958" y2="5426">
     </linearGradient>
     <linearGradient id="id33" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="4619" y1="4198" x2="5850" y2="4198">
     </linearGradient>
     <linearGradient id="id34" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="4619" y1="3883" x2="5850" y2="3883">
     </linearGradient>
     <linearGradient id="id35" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="3112" y1="9205" x2="3690" y2="9205">
     </linearGradient>
     <linearGradient id="id36" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="1262" y1="4208" x2="1989" y2="4208">
     </linearGradient>
     <linearGradient id="id37" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="3498" y1="8696" x2="3768" y2="8696">
     </linearGradient>
     <linearGradient id="id38" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="6814" y1="9205" x2="6698" y2="10979">
     </linearGradient>
     <linearGradient id="id39" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="668" y1="3090" x2="1261" y2="3090">
     </linearGradient>
     <linearGradient id="id40" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="3536" y1="4457" x2="6004" y2="4457">
     </linearGradient>
     <linearGradient id="id41" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="3424" y1="6698" x2="3729" y2="6698">
     </linearGradient>
     <linearGradient id="id42" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="453" y1="4283" x2="679" y2="4283">
     </linearGradient>
     <linearGradient id="id43" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="2688" y1="8289" x2="3189" y2="8289">
     </linearGradient>
     <linearGradient id="id44" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="6006" y1="3614" x2="7238" y2="3614">
     </linearGradient>
     <linearGradient id="id45" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="6006" y1="3269" x2="7238" y2="3269">
     </linearGradient>
     <linearGradient id="id46" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="6" y1="2156" x2="1938" y2="2156">
     </linearGradient>
     <linearGradient id="id47" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="4269" y1="3771" x2="5464" y2="3771">
     </linearGradient>
     <linearGradient id="id48" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="15342" y1="1779" x2="17179" y2="1779">
     </linearGradient>
     <linearGradient id="id49" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="15342" y1="2480" x2="17179" y2="2480">
     </linearGradient>
     <linearGradient id="id50" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="15342" y1="3143" x2="17179" y2="3143">
     </linearGradient>
     <linearGradient id="id51" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="951" y1="1399" x2="951" y2="2463">
     </linearGradient>
     <linearGradient id="id52" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="15529" y1="2718" x2="16994" y2="2718">
     </linearGradient>
     <linearGradient id="id53" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="15529" y1="1999" x2="16994" y2="1999">
     </linearGradient>
     <linearGradient id="id54" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="3960" y1="3151" x2="6737" y2="1184">
     </linearGradient>
     <linearGradient id="id55" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="2148" y1="6467" x2="4924" y2="6467">
     </linearGradient>
     <linearGradient id="id56" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="5966" y1="3442" x2="7277" y2="3442">
     </linearGradient>
     <linearGradient id="id57" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="5966" y1="3089" x2="7277" y2="3089">
     </linearGradient>
     <linearGradient id="id58" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="5233" y1="3883" x2="8009" y2="1917">
     </linearGradient>
     <linearGradient id="id59" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13292" y1="5936" x2="14873" y2="5936">
     </linearGradient>
     <linearGradient id="id60" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13292" y1="5493" x2="14873" y2="5493">
     </linearGradient>
     <linearGradient id="id61" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="6120" y1="8164" x2="6081" y2="10670">
     </linearGradient>
     <linearGradient id="id62" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="6120" y1="8356" x2="6081" y2="10130">
     </linearGradient>
     <linearGradient id="id63" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="8703" y1="9552" x2="8703" y2="12058">
     </linearGradient>
     <linearGradient id="id64" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="8943" y1="3037" x2="10430" y2="3037">
     </linearGradient>
     <linearGradient id="id65" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="8943" y1="3478" x2="10430" y2="3478">
     </linearGradient>
     <linearGradient id="id66" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="8896" y1="2789" x2="10477" y2="2789">
     </linearGradient>
     <linearGradient id="id67" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="8896" y1="3235" x2="10477" y2="3235">
     </linearGradient>
     <linearGradient id="id68" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="3536" y1="8588" x2="4230" y2="8588">
     </linearGradient>
     <linearGradient id="id69" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="13342" y1="5708" x2="14826" y2="5708">
     </linearGradient>
     <linearGradient id="id70" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="13342" y1="6147" x2="14826" y2="6147">
     </linearGradient>
     <linearGradient id="id71" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="7431" y1="9051" x2="7431" y2="9937">
     </linearGradient>
     <linearGradient id="id72" gradientUnits="userSpaceOnUse" xlink:href="#id26" x1="14025" y1="1080" x2="14604" y2="1080">
     </linearGradient>
     <linearGradient id="id73" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="14025" y1="2439" x2="14604" y2="2439">
     </linearGradient>
     <linearGradient id="id74" gradientUnits="userSpaceOnUse" xlink:href="#id26" x1="14025" y1="1767" x2="14604" y2="1767">
     </linearGradient>
     <linearGradient id="id75" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13918" y1="1038" x2="14712" y2="1038">
     </linearGradient>
     <linearGradient id="id76" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13918" y1="2423" x2="14712" y2="2423">
     </linearGradient>
     <linearGradient id="id77" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="15884" y1="3471" x2="16640" y2="3471">
     </linearGradient>
     <linearGradient id="id78" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13918" y1="1750" x2="14712" y2="1750">
     </linearGradient>
     <linearGradient id="id79" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="15884" y1="2086" x2="16640" y2="2086">
     </linearGradient>
     <linearGradient id="id80" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="15884" y1="2798" x2="16640" y2="2798">
     </linearGradient>
     <linearGradient id="id81" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="821" y1="4307" x2="1798" y2="4307">
     </linearGradient>
     <linearGradient id="id82" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="3845" y1="8202" x2="4230" y2="8202">
     </linearGradient>
     <linearGradient id="id83" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="8164" y1="9282" x2="8048" y2="11056">
     </linearGradient>
     <linearGradient id="id84" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="8816" y1="5515" x2="11986" y2="5515">
     </linearGradient>
     <linearGradient id="id85" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="4269" y1="2688" x2="5811" y2="2071">
     </linearGradient>
     <linearGradient id="id86" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="5541" y1="3420" x2="7084" y2="2803">
     </linearGradient>
     <linearGradient id="id87" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="8742" y1="9320" x2="8665" y2="11094">
     </linearGradient>
     <linearGradient id="id88" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="2960" y1="7628" x2="4182" y2="7628">
     </linearGradient>
     <linearGradient id="id89" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="7470" y1="8935" x2="7431" y2="10786">
     </linearGradient>
     <linearGradient id="id90" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13376" y1="1448" x2="15253" y2="1448">
     </linearGradient>
     <linearGradient id="id91" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13376" y1="2111" x2="15253" y2="2111">
     </linearGradient>
     <linearGradient id="id92" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="13376" y1="785" x2="15253" y2="785">
     </linearGradient>
     <linearGradient id="id93" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="13562" y1="1659" x2="15066" y2="1659">
     </linearGradient>
     <linearGradient id="id94" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="13562" y1="941" x2="15066" y2="941">
     </linearGradient>
     <linearGradient id="id95" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="8086" y1="9359" x2="8048" y2="11827">
     </linearGradient>
     <linearGradient id="id96" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="13387" y1="1080" x2="15242" y2="1080">
     </linearGradient>
     <linearGradient id="id97" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="13387" y1="1743" x2="15242" y2="1743">
     </linearGradient>
     <linearGradient id="id98" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="13387" y1="2406" x2="15242" y2="2406">
     </linearGradient>
     <linearGradient id="id99" gradientUnits="userSpaceOnUse" xlink:href="#id22" x1="12386" y1="8337" x2="14546" y2="8337">
     </linearGradient>
     <linearGradient id="id100" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="5464" y1="3883" x2="5966" y2="2572">
     </linearGradient>
     <linearGradient id="id101" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="629" y1="4090" x2="1972" y2="4090">
     </linearGradient>
     <linearGradient id="id102" gradientUnits="userSpaceOnUse" xlink:href="#id26" x1="1994" y1="6467" x2="4770" y2="6467">
     </linearGradient>
     <linearGradient id="id103" gradientUnits="userSpaceOnUse" xlink:href="#id18" x1="7470" y1="7585" x2="7470" y2="9475">
     </linearGradient>
     <linearGradient id="id104" gradientUnits="userSpaceOnUse" xlink:href="#id15" x1="11596" y1="9797" x2="15876" y2="9797">
     </linearGradient>
     <linearGradient id="id105" gradientUnits="userSpaceOnUse" xlink:href="#id13" x1="11964" y1="10718" x2="15877" y2="10718">
     </linearGradient>
 </defs>
                        <g id="Layer_x0020_1">
                            <metadata id="CorelCorpID_0Corel-Layer"/>
                            <g id="_228345345376">
                                <g>
                                    <g>
                                        <g>
                                            <path class="fil0" d="M17206 11079l0 0 0 -274 -405 0 -386 -224c-85,-50 -193,-50 -282,0l-386 224 -401 0 0 274 0 0c-8,42 8,89 50,112l740 428c85,50 193,50 278,0l737 -424c46,-27 66,-73 54,-116z"/>
                                            <path class="fil1" d="M15406 10886l740 428c85,50 193,50 278,0l737 -424c69,-39 69,-143 0,-181l-744 -436c-85,-50 -193,-50 -282,0l-733 428c-66,42 -66,143 4,185z"/>
                                            <g>
                                                <path class="fil2" d="M16339 10751l228 131 85 -50c31,-15 31,-58 0,-77l-312 -181c-35,-19 -81,-19 -116,0l-308 181c-31,19 -31,58 0,77l85 50 224 -131c35,-23 77,-23 116,0z"/>
                                                <path class="fil3" d="M16339 11013l224 -131 -228 -131c-35,-19 -81,-19 -116,0l-224 131 228 131c35,19 81,19 116,0z"/>
                                            </g>
                                        </g>
                                        <path class="fil4" d="M17025 10277l-1489 0 0 521 0 0c0,27 15,58 42,73l594 343c69,39 154,39 224,0l590 -339c31,-15 42,-46 42,-77l0 0 0 -521 -4 0z"/>
                                    </g>
                                    <g>
                                        <g>
                                            <path class="fil5" d="M17206 10369l0 0 0 -274 -405 0 -386 -224c-85,-50 -193,-50 -282,0l-386 224 -401 0 0 274 0 0c-8,42 8,89 50,112l740 428c85,50 193,50 278,0l737 -424c46,-27 66,-73 54,-116z"/>
                                            <path class="fil6" d="M15406 10177l740 428c85,50 193,50 278,0l737 -424c69,-39 69,-143 0,-181l-744 -432c-85,-50 -193,-50 -282,0l-733 428c-66,42 -66,143 4,181z"/>
                                            <g>
                                                <path class="fil7" d="M16339 10042l228 131 85 -50c31,-15 31,-58 0,-77l-312 -181c-35,-19 -81,-19 -116,0l-308 181c-31,19 -31,58 0,77l85 50 224 -131c35,-19 77,-19 116,0z"/>
                                                <path class="fil8" d="M16339 10304l224 -131 -228 -131c-35,-19 -81,-19 -116,0l-224 131 228 131c35,19 81,19 116,0z"/>
                                            </g>
                                        </g>
                                        <path class="fil9" d="M17025 9567l-1489 0 0 521 0 0c0,27 15,58 42,73l594 343c69,39 154,39 224,0l590 -339c31,-15 42,-46 42,-77l0 0 0 -521 -4 0z"/>
                                    </g>
                                    <g>
                                        <path class="fil10" d="M17206 9660l0 0 0 -274 -405 0 -386 -224c-85,-50 -193,-50 -282,0l-386 224 -401 0 0 274 0 0c-8,42 8,89 50,112l740 428c85,50 193,50 278,0l737 -424c46,-27 66,-73 54,-116z"/>
                                        <path class="fil11" d="M15406 9467l740 428c85,50 193,50 278,0l737 -424c69,-39 69,-143 0,-181l-744 -432c-85,-50 -193,-50 -282,0l-733 428c-66,39 -66,143 4,181z"/>
                                        <g>
                                            <path class="fil12" d="M16339 9332l228 131 85 -50c31,-15 31,-58 0,-77l-312 -181c-35,-19 -81,-19 -116,0l-308 181c-31,19 -31,58 0,77l85 50 224 -131c35,-19 77,-19 116,0z"/>
                                            <path class="fil13" d="M16339 9594l224 -131 -228 -131c-35,-19 -81,-19 -116,0l-224 131 228 131c35,19 81,19 116,0z"/>
                                        </g>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <g>
                                            <path class="fil14" d="M15263 12147l0 0 0 -274 -405 0 -386 -224c-85,-50 -193,-50 -282,0l-386 224 -397 0 0 274 0 0c-8,42 8,89 50,112l740 428c85,50 193,50 278,0l737 -424c42,-27 58,-73 50,-116z"/>
                                            <path class="fil15" d="M13458 11954l740 428c85,50 193,50 278,0l737 -424c69,-39 69,-143 0,-181l-740 -432c-85,-50 -193,-50 -282,0l-733 428c-69,42 -69,143 0,181z"/>
                                            <g>
                                                <path class="fil16" d="M14391 11819l228 131 85 -50c31,-15 31,-58 0,-77l-312 -181c-35,-19 -81,-19 -116,0l-308 181c-31,19 -31,58 0,77l85 50 224 -131c35,-19 81,-19 116,0z"/>
                                                <path class="fil17" d="M14395 12082l224 -131 -228 -131c-35,-19 -81,-19 -116,0l-224 131 228 131c35,19 81,19 116,0z"/>
                                            </g>
                                        </g>
                                        <path class="fil18" d="M15082 11345l-1489 0 0 521 0 0c0,27 15,58 42,73l594 343c69,39 154,39 224,0l590 -339c31,-15 42,-46 42,-77l0 0 0 -521 -4 0z"/>
                                    </g>
                                    <g>
                                        <g>
                                            <path class="fil19" d="M15263 11441l0 0 0 -274 -405 0 -386 -224c-85,-50 -193,-50 -282,0l-386 224 -397 0 0 274 0 0c-8,42 8,89 50,112l740 428c85,50 193,50 278,0l737 -424c42,-27 58,-73 50,-116z"/>
                                            <path class="fil20" d="M13458 11249l740 428c85,50 193,50 278,0l737 -424c69,-39 69,-143 0,-181l-740 -432c-85,-50 -193,-50 -282,0l-733 428c-69,39 -69,139 0,181z"/>
                                            <g>
                                                <path class="fil21" d="M14391 11114l228 131 85 -50c31,-15 31,-58 0,-77l-312 -181c-35,-19 -81,-19 -116,0l-308 181c-31,19 -31,58 0,77l85 50 224 -131c35,-23 81,-23 116,0z"/>
                                                <path class="fil22" d="M14395 11376l224 -131 -228 -131c-35,-19 -81,-19 -116,0l-224 131 228 131c35,19 81,19 116,0z"/>
                                            </g>
                                        </g>
                                        <path class="fil23" d="M15082 10635l-1489 0 0 521 0 0c0,27 15,58 42,73l594 343c69,39 154,39 224,0l590 -339c31,-15 42,-46 42,-77l0 0 0 -521 -4 0z"/>
                                    </g>
                                    <g>
                                        <path class="fil24" d="M15263 10732l0 0 0 -274 -405 0 -386 -224c-85,-50 -193,-50 -282,0l-386 224 -397 0 0 274 0 0c-8,42 8,89 50,112l740 428c85,50 193,50 278,0l737 -424c42,-27 58,-77 50,-116z"/>
                                        <path class="fil25" d="M13458 10539l740 428c85,50 193,50 278,0l737 -424c69,-39 69,-143 0,-181l-740 -432c-85,-50 -193,-50 -282,0l-733 428c-69,39 -69,139 0,181z"/>
                                        <g>
                                            <path class="fil26" d="M14391 10404l228 131 85 -50c31,-15 31,-58 0,-77l-312 -181c-35,-19 -81,-19 -116,0l-308 181c-31,19 -31,58 0,77l85 50 224 -131c35,-23 81,-23 116,0z"/>
                                            <path class="fil27" d="M14395 10662l224 -131 -228 -131c-35,-19 -81,-19 -116,0l-224 131 228 131c35,23 81,23 116,0z"/>
                                        </g>
                                    </g>
                                </g>
                            </g>
                            <g id="_228345290240">
                                <path class="fil28" d="M14908 7466l0 0 0 -282 -185 0c-393,-220 -872,-224 -1269,-8 -4,4 -8,4 -12,8l-139 0 0 282 0 0c8,89 58,174 147,228l23 15c393,228 875,235 1276,15l4 0c100,-58 154,-158 154,-258z"/>
                                <path class="fil29" d="M13450 7427l23 15c393,228 875,235 1276,15l4 0c204,-108 208,-397 8,-513l-19 -12c-397,-231 -883,-239 -1288,-19l0 0c-201,108 -204,397 -4,513z"/>
                            </g>
                            <path class="fil30" d="M14908 6895l0 0 0 -282 -185 0c-393,-220 -872,-224 -1269,-8 -4,4 -8,4 -12,8l-139 0 0 282 0 0c8,89 58,174 147,228l23 15c393,228 875,235 1276,15l4 0c100,-58 154,-158 154,-258z"/>
                            <path class="fil31" d="M13450 6856l23 15c393,228 875,235 1276,15l4 0c204,-108 208,-397 8,-513l-19 -12c-397,-231 -883,-239 -1288,-19l0 0c-201,112 -204,401 -4,513z"/>
                            <g id="_228345292736">
                                <path class="fil32" d="M10504 10967l0 0 0 -282 -185 0c-393,-220 -872,-224 -1269,-8 -4,4 -8,4 -12,8l-139 0 0 282 0 0c8,89 58,174 147,228l23 15c393,228 875,231 1276,19l4 0c104,-62 154,-162 154,-262z"/>
                                <path class="fil33" d="M9047 10929l23 15c393,228 875,231 1276,19l4 0c204,-108 208,-397 8,-513l-19 -12c-397,-231 -883,-239 -1288,-19l0 0c-201,108 -201,397 -4,509z"/>
                            </g>
                            <path class="fil34" d="M10504 10400l0 0 0 -282 -185 0c-393,-220 -872,-224 -1269,-8 -4,4 -8,4 -12,8l-139 0 0 282 0 0c8,89 58,174 147,228l23 15c393,228 875,231 1276,19l4 0c104,-62 154,-162 154,-262z"/>
                            <path class="fil35" d="M9047 10362l23 15c393,228 875,231 1276,19l4 0c204,-108 208,-397 8,-513l-19 -12c-397,-231 -883,-239 -1288,-19l0 0c-201,104 -201,393 -4,509z"/>
                            <g id="_228345291936">
                                <path class="fil36" d="M10713 7215l1076 636 143 -81c131,-77 135,-266 0,-343l-1222 -725c-170,-100 -382,-104 -555,-8l-1257 698c-135,73 -139,270 -4,347l147 85 1118 -617c177,-96 386,-93 555,8z"/>
                                <path class="fil37" d="M10720 8472l1072 -617 -1076 -636c-170,-100 -382,-104 -555,-8l-1118 617 1118 648c174,96 386,96 559,-4z"/>
                            </g>
                            <path class="fil38" d="M10693 7766c0,-87 -116,-158 -258,-158 -143,0 -258,71 -258,158 0,87 116,158 258,158 143,0 258,-71 258,-158z"/>
                            <polygon class="fil39" points="5634,4851 6205,5179 9320,3393 8754,3081 "/>
                            <polygon class="fil40" points="8075,702 8098,683 8098,3378 8075,3401 "/>
                            <polygon class="fil41" points="6768,1905 6791,1890 6791,3883 6768,3906 "/>
                            <polygon class="fil42" points="8703,497 8727,482 8727,3178 8703,3201 "/>
                            <polygon class="fil43" points="7454,1805 7477,1785 7477,3779 7454,3802 "/>
                            <polygon class="fil44" points="6112,1878 6135,1863 6135,4558 6112,4581 "/>
                            <polygon class="fil45" points="7886,1411 8310,1172 8310,2869 7886,3112 "/>
                            <polygon class="fil46" points="6579,2591 7003,2348 7003,3382 6579,3629 "/>
                            <polygon class="fil47" points="8503,1936 8931,1697 8931,2726 8503,2973 "/>
                            <polygon class="fil48" points="7242,2838 7666,2599 7666,3397 7242,3644 "/>
                            <polygon class="fil49" points="5912,2684 6336,2445 6336,4142 5912,4388 "/>
                            <g id="_228345295520">
                                <path class="fil50" d="M14588 4527l0 -224 -289 0 -706 -405c-66,-39 -147,-39 -212,0l-706 405 -289 0 0 235c0,39 19,77 58,100l937 540c66,39 147,39 212,0l937 -540c42,-23 62,-69 58,-112z"/>
                                <path class="fil51" d="M12448 4396l937 540c66,39 147,39 212,0l937 -540c77,-46 77,-158 0,-201l-937 -540c-66,-39 -147,-39 -212,0l-937 540c-77,42 -77,154 0,201z"/>
                            </g>
                            <path class="fil52" d="M13489 2992c0,0 12,216 31,282 19,66 85,748 81,779 -4,31 -27,266 -42,305 39,23 131,39 131,39l39 -35 15 -158 4 -120c0,0 -12,-35 -15,-35 -4,-4 -15,-455 -15,-540 0,-85 -15,-247 -12,-293 4,-46 19,-189 19,-189l-235 -35z"/>
                            <path class="fil52" d="M13211 2869c0,0 0,201 27,328 27,127 150,675 154,744 4,69 19,231 -69,320 19,66 73,62 73,62l58 -50 39 -104 77 -93c0,0 0,-23 -8,-31 -69,-262 -85,-609 -100,-706 -19,-100 -27,-135 -31,-170 -4,-39 27,-258 27,-258l-247 -42z"/>
                            <g id="_228345298656">
                                <path class="fil53" d="M13273 4354c0,15 0,27 8,31 77,42 174,-31 185,-46 4,-4 4,-12 4,-23 -35,27 -85,54 -139,58 -39,4 -54,-8 -58,-19z"/>
                                <path class="fil53" d="M13582 4130c0,0 -19,23 -19,54 0,31 0,93 0,93 0,0 -4,12 -23,4 0,-27 15,-120 -54,-77 8,-35 69,-104 96,-73z"/>
                                <path class="fil54" d="M13562 4041c19,19 -89,96 -116,185 -8,35 -35,85 -54,69 -15,-19 -46,-42 -73,-39 -35,31 -50,69 -50,96 8,12 23,23 58,19 54,-4 108,-31 139,-58 0,-31 -4,-81 15,-112 23,-42 89,-46 96,-73 12,-66 -15,-89 -15,-89z"/>
                            </g>
                            <g id="_228345300416">
                                <path class="fil54" d="M13747 4230c-4,-81 19,-147 -15,-174 15,35 15,197 -19,312 -12,19 -19,19 -27,15 -8,-4 -58,-50 -127,-23 0,0 -12,66 4,127 8,12 15,19 27,19 27,4 89,-19 150,-131 8,-46 8,-93 8,-147z"/>
                                <path class="fil53" d="M13585 4504c-8,0 -19,-12 -27,-19 0,8 4,15 8,23 27,66 127,-23 150,-77 8,-15 12,-35 19,-54 -62,108 -120,131 -150,127z"/>
                            </g>
                            <path class="fil53" d="M13616 474c0,0 -297,-108 -374,154 -77,262 -19,382 23,432 39,-50 420,154 386,197 66,-15 212,-50 212,-189 0,-139 -62,-162 -69,-255 -15,-100 39,-297 -177,-339z"/>
                            <path class="fil52" d="M13836 1712c0,0 35,220 35,324 0,104 -27,656 -39,690 -15,35 -42,104 -27,108 15,4 42,-46 42,-46 0,0 -23,73 -12,93 12,19 54,35 96,23 42,-15 27,-139 19,-193 4,-81 77,-555 77,-648 0,-93 -31,-362 -31,-362 0,0 -123,-35 -162,12z"/>
                            <g id="_228345300160">
                                <g>
                                    <path class="fil52" d="M12984 1253c0,0 -104,266 -135,255 -31,-12 -216,-154 -208,-201 4,-46 35,-93 50,-108 15,-15 -23,-39 -58,19 -4,-58 -19,-189 -23,-224 0,-35 -35,-23 -35,31 0,54 0,85 0,85 0,0 -66,-19 -85,39 -19,58 15,174 46,197 12,39 197,339 355,343 58,-31 131,-127 228,-343 -77,-66 -116,-93 -135,-93z"/>
                                    <path class="fil55" d="M12486 1195c0,0 15,35 100,-8 -8,19 -85,58 -100,8z"/>
                                </g>
                                <rect class="fil56" x="12444" y="683" width="1226" height="1226"/>
                            </g>
                            <path class="fil54" d="M13987 1481c-62,-131 -39,-189 -120,-220 -81,-31 -189,-100 -251,-143 27,19 58,50 58,73 4,35 -35,123 -181,100 -147,-19 -166,-150 -139,-185 23,-54 15,-108 15,-108 0,0 -154,-112 -220,-73 -23,4 -73,108 -127,166 -50,42 -93,104 -46,181 54,93 143,104 143,104l0 0c0,93 46,177 77,228 35,62 35,255 0,301 -170,139 -12,1006 -12,1006 0,0 139,93 305,158 170,66 235,-35 235,-35 0,0 4,-100 85,-463 46,-208 23,-316 -8,-405l0 0c0,0 -96,-120 -112,-274 -15,-154 112,-270 112,-270 -8,23 8,81 35,89 54,19 143,8 170,-19 31,-42 31,-104 -19,-212z"/>
                            <path class="fil57" d="M13693 1893c15,154 112,274 112,274l0 0c-23,-66 -54,-120 -66,-201 -15,-139 42,-262 66,-339 0,0 0,-4 0,-4 -4,0 -127,112 -112,270z"/>
                            <path class="fil55" d="M13597 1107c4,4 12,8 19,12 -8,-4 -15,-12 -19,-12z"/>
                            <g id="_228345301920">
                                <path class="fil58" d="M13504 1165c39,0 62,-35 77,-73 0,0 0,0 0,0 -12,-35 12,-81 12,-81l-162 -8 0 19c-8,0 -23,143 73,143z"/>
                                <path class="fil52" d="M13674 1192c0,-27 -31,-54 -58,-73 -8,-4 -12,-8 -19,-12 -8,-4 -12,-8 -19,-12 -12,42 -35,77 -77,73 -96,0 -77,-147 -77,-147l0 -19 -58 -4c0,0 8,58 -15,108 -23,35 -4,166 139,185 150,23 189,-66 185,-100z"/>
                            </g>
                            <path class="fil52" d="M13424 559c0,0 -35,224 197,320 31,-42 85,-42 81,27 0,39 -73,46 -73,46 0,0 -50,89 -131,120 -81,31 -201,19 -224,-46 -23,-66 -73,-228 15,-432 69,12 135,-35 135,-35z"/>
                            <path class="fil57" d="M13115 1373c0,0 0,-58 42,-108 -23,8 -58,104 -58,104 0,0 12,4 15,4z"/>
                            <path class="fil59" d="M13801 1623c0,0 0,4 0,4 0,-4 0,-4 0,-4z"/>
                            <g id="_228345304928">
                                <path class="fil60" d="M13161 2121c-675,-324 -1500,-837 -1496,-1677l0 -4 -54 2136c0,0 428,1955 4284,1735l0 -1874c0,0 -1647,204 -2734,-316z"/>
                                <path class="fil61" d="M13196 1974c-1890,-802 -1288,-1959 -1288,-1959l-174 -15c-50,158 -73,305 -73,443 0,837 821,1354 1496,1677 1091,521 2734,312 2734,312l0 -50 0 -77c-1539,208 -2695,-332 -2695,-332z"/>
                            </g>
                            <g id="_228345305280">
                                <path class="fil62 str0" d="M12097 2722c0,0 366,474 937,648"/>
                                <path class="fil62 str0" d="M13150 3390c0,-45 -26,-81 -58,-81 -32,0 -58,36 -58,81 0,45 26,81 58,81 32,0 58,-36 58,-81z"/>
                                <path class="fil63 str1" d="M12109 2352c0,0 239,343 648,571"/>
                                <path class="fil63 str1" d="M12864 2950c0,-45 -26,-81 -58,-81 -32,0 -58,36 -58,81 0,45 26,81 58,81 32,0 58,-36 58,-81z"/>
                                <path class="fil63 str1" d="M12120 1974c0,0 177,282 497,521"/>
                                <path class="fil63 str1" d="M12718 2510c0,-45 -26,-81 -58,-81 -32,0 -58,36 -58,81 0,45 26,81 58,81 32,0 58,-36 58,-81z"/>
                            </g>
                            <rect class="fil53" x="14129" y="3478" width="212" height="351"/>
                            <rect class="fil53" x="15147" y="2815" width="197" height="1037"/>
                            <rect class="fil53" x="14823" y="3478" width="189" height="374"/>
                            <rect class="fil53" x="14484" y="3259" width="193" height="594"/>
                            <path class="fil64" d="M3085 4566l0 0c-93,-58 -216,-66 -328,-4l-1677 968c-197,112 -320,324 -320,551l0 2838 2788 1620 235 -578 23 -906 2090 -2857 -2811 -1631z"/>
                            <path class="fil65" d="M3548 10543l0 -2838c0,-228 123,-440 320,-551l1677 -968c216,-123 482,31 482,278l0 2626 -2480 1454z"/>
                            <g id="_228345307552">
                                <path class="fil66" d="M4277 10107l0 -1284c0,-166 89,-316 231,-401l737 -424c104,-62 231,15 231,135l0 1273 -1199 702z"/>
                                <path class="fil67" d="M4277 10107l0 -1284c0,-166 89,-316 231,-401l737 -424c104,-62 231,15 231,135l0 1273 -1199 702z"/>
                            </g>
                            <g id="_228345308096">
                                <polygon class="fil54" points="4234,6417 4789,6112 2931,5036 2001,5576 2715,5989 3100,5765 "/>
                                <polygon class="fil68" points="4234,6417 3100,5765 2715,5989 3837,6637 "/>
                                <polygon class="fil69" points="4234,6417 3100,5765 2715,5989 3837,6637 "/>
                            </g>
                            <polygon class="fil70" points="6675,11573 8029,10778 5545,9371 4196,10142 "/>
                            <g id="_228345310624">
                                <polygon class="fil71" points="5468,8684 5549,8630 7099,9525 8029,10778 7948,10832 "/>
                                <polygon class="fil72" points="5468,9398 5468,8684 7018,9579 7948,10832 "/>
                            </g>
                            <g id="_228345310368">
                                <path class="fil73" d="M5900 9853l0 0 0 -224 -150 0c-316,-177 -698,-181 -1018,-8 -4,0 -8,4 -12,8l-112 0 0 224 0 0c8,73 46,139 116,181l19 12c316,181 706,189 1026,15l4 0c85,-50 123,-127 127,-208z"/>
                                <path class="fil74" d="M4728 9822l19 12c316,181 706,189 1026,15l4 0c162,-89 166,-320 8,-413l-15 -8c-320,-185 -710,-193 -1033,-15l0 0c-166,81 -170,316 -8,409z"/>
                            </g>
                            <g id="_228345312896">
                                <path class="fil75" d="M7292 10628l0 0 0 -224 -150 0c-316,-177 -698,-181 -1018,-8 -4,0 -8,4 -12,8l-108 0 0 224 0 0c8,73 46,139 116,181l19 12c316,181 706,189 1026,15l4 0c81,-46 123,-127 123,-208z"/>
                                <path class="fil76" d="M6120 10597l19 12c316,181 706,189 1026,15l4 0c162,-89 166,-320 8,-413l-15 -8c-320,-185 -710,-193 -1033,-15l0 0c-166,85 -166,320 -8,409z"/>
                            </g>
                            <g id="_228345313056">
                                <polygon class="fil77" points="4196,9425 4277,9371 5827,10265 6756,11519 6675,11573 "/>
                                <polygon class="fil78" points="4196,10142 4196,9425 5746,10319 6675,11573 "/>
                            </g>
                            <g id="_228345313600">
                                <path class="fil79" d="M1963 10863l0 -201 -258 0 -629 -362c-58,-35 -131,-35 -189,0l-629 362 -258 0 0 208c0,35 19,69 50,89l833 482c58,35 131,35 189,0l833 -482c42,-15 62,-58 58,-96z"/>
                                <path class="fil80" d="M54 10747l833 482c58,35 131,35 189,0l837 -482c69,-39 69,-139 0,-177l-833 -482c-58,-35 -131,-35 -189,0l-833 482c-73,35 -73,135 -4,177z"/>
                            </g>
                            <g id="_228345315360">
                                <path class="fil81" d="M1222 7936c23,-50 19,-139 -4,-228 23,-19 39,-31 39,-31 0,0 62,77 35,143 -15,31 -12,42 0,69 12,23 -19,185 -35,185 -12,4 -31,0 -50,-4 -4,-4 -27,-50 15,-135z"/>
                                <path class="fil82" d="M949 8017c0,-4 0,-4 0,-8 0,-81 185,-235 266,-301 23,93 31,181 4,228 -42,85 -15,131 -15,131 -27,-4 -54,-8 -58,0 -8,12 -27,54 -31,69 -4,15 -12,66 -12,66 -58,-120 -112,-162 -154,-185z"/>
                                <path class="fil81" d="M949 8017c46,23 100,66 158,185 0,0 -93,31 -201,-39 8,-27 31,-62 42,-147z"/>
                                <path class="fil83" d="M1238 7709c0,0 19,89 -8,123 -31,35 -35,108 -35,108l-19 12c0,0 4,-54 -27,-50 -31,4 -89,112 -154,108 -35,-4 -42,-12 -42,-12 0,0 -8,-31 -27,-50 -19,-19 -116,-189 -27,-301 104,8 339,62 339,62z"/>
                                <path class="fil84" d="M898 7639c0,0 54,-89 150,-77 96,12 177,39 224,35 46,-4 31,100 -39,135 -69,31 -378,39 -335,-93z"/>
                            </g>
                            <g id="_228345316256">
                                <g>
                                    <path class="fil85" d="M979 10597c0,0 -8,108 -15,135 35,42 150,54 197,0 19,-19 -12,-39 -35,-50 -19,-12 -8,-73 -8,-73l-139 -12z"/>
                                    <g>
                                        <path class="fil86" d="M1404 10697l-4 0c0,0 -4,-4 -4,-4 -12,-8 -77,15 -158,42l-301 73 0 12 0 0c0,4 4,15 15,23 31,23 96,19 139,4 42,-15 85,-23 108,-23 27,0 96,-8 139,-42 35,-27 66,-54 66,-73l0 0 0 -12zm-432 120l139 -35c-54,19 -108,31 -139,35z"/>
                                        <path class="fil87" d="M941 10801c0,0 -23,12 8,35 31,23 96,19 139,4 42,-15 85,-23 108,-23 23,0 96,-8 139,-42 42,-35 85,-69 58,-93 -27,-19 -382,147 -451,120z"/>
                                        <path class="fil88" d="M964 10713c0,0 -27,15 -27,96 19,19 54,35 104,27 50,-8 93,-27 123,-31 31,-4 108,-8 150,-35 42,-27 96,-66 69,-93 0,0 0,-12 -50,-4 -50,8 -154,15 -212,8 12,12 35,81 -77,81 -23,0 -77,-23 -81,-50z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path class="fil86" d="M671 10593c0,0 -15,31 8,58 23,27 93,15 116,-35 23,-50 39,-69 54,-77 15,-8 89,-35 108,-66 19,-31 12,-81 -12,-85l-274 204zm0 0l0 0 0 0z"/>
                                    </g>
                                    <g>
                                        <path class="fil87" d="M671 10578c0,0 -15,31 8,58 23,27 93,15 116,-35 23,-50 39,-69 54,-77 15,-8 89,-35 108,-66 19,-31 12,-81 -12,-85l-274 204zm0 0l0 0 0 0z"/>
                                    </g>
                                    <g>
                                        <path class="fil88" d="M683 10531c0,0 -15,4 -8,77 12,19 46,42 89,12 42,-31 35,-69 66,-89 31,-19 93,-35 112,-73 19,-39 27,-81 0,-85 -27,-4 -112,35 -112,35l-147 123zm0 0l0 0 0 0z"/>
                                    </g>
                                    <path class="fil85" d="M686 10454l-4 81c0,0 4,31 39,31 35,0 77,-19 85,-96 -23,-19 -120,-15 -120,-15z"/>
                                </g>
                                <g>
                                    <path class="fil87" d="M922 9718c4,-154 0,-382 0,-382l-77 -46 77 428z"/>
                                    <path class="fil89" d="M659 8970c0,0 -39,73 12,228 19,58 58,532 50,571 -8,39 -42,413 -50,698 39,35 104,58 154,-12 12,-123 77,-544 93,-594 4,-15 8,-73 8,-143l-77 -428 77 46 77 50c0,0 35,490 31,521 -23,54 -62,702 -62,702 0,0 81,108 154,0 15,-147 108,-632 116,-713 8,-81 27,-698 27,-698l-609 -228z"/>
                                </g>
                            </g>
                            <path class="fil81" d="M470 9039c0,0 -4,81 -8,100 -4,19 -19,58 4,77 23,19 93,15 100,-89 4,27 4,35 4,42 0,8 12,8 15,-12 4,-19 -8,-96 -8,-96l-108 -23z"/>
                            <path class="fil53" d="M467 9027c0,0 19,54 62,54 42,0 69,-31 69,-31l0 -77 -123 -27 -8 81z"/>
                            <polygon class="fil53" points="737,9000 910,9120 852,8754 "/>
                            <path class="fil54" d="M1242 9274c42,-27 66,-69 62,-77 -66,-216 -19,-725 -19,-725 0,0 -27,27 -50,-8 -62,332 -15,667 8,810z"/>
                            <path class="fil90" d="M683 8013c-73,100 -154,401 -170,455 -19,66 -39,359 -42,413 -4,54 -4,73 15,100 54,42 104,0 116,-15 12,-15 46,-405 46,-428 4,-23 19,-39 35,-73 4,0 -46,-93 0,-451z"/>
                            <path class="fil53" d="M891 8006c-31,12 -31,50 -19,69 12,23 116,81 162,89 50,4 100,23 131,31 -120,-135 -258,-193 -274,-189z"/>
                            <g>
                                <path class="fil91" d="M1920 6976l949 548c66,39 104,104 104,177l0 4c0,108 -116,174 -208,120l-949 -548c-66,-39 -108,-112 -104,-185l0 0c4,-104 120,-170 208,-116z"/>
                                <path class="fil54" d="M1654 6953l0 0c-4,-73 -42,-135 -104,-170l-12 -8c-89,-54 -204,12 -204,116l0 0c0,77 46,150 116,185l8 4c93,42 201,-27 197,-127z"/>
                            </g>
                            <g>
                                <path class="fil92" d="M1920 7701l949 548c66,39 104,104 104,177l0 4c0,108 -116,174 -208,120l-949 -548c-66,-39 -108,-112 -104,-185l0 0c4,-104 120,-170 208,-116z"/>
                                <path class="fil54" d="M1654 7674l0 0c-4,-73 -42,-135 -104,-170l-12 -8c-89,-54 -204,12 -204,116l0 0c0,77 46,150 116,185l8 4c93,46 201,-23 197,-127z"/>
                            </g>
                            <g>
                                <path class="fil93" d="M2722 3687c0,0 243,-174 478,-127l23 212 -262 19 -239 -104z"/>
                                <path class="fil94" d="M3729 3791l0 -228 -185 108 -12 328c0,0 0,135 23,116 23,-23 212,-220 212,-220l-39 -104z"/>
                                <path class="fil95" d="M4265 4253c0,0 -389,-362 -540,-467l-185 116 0 150 189 181 204 143 104 23 50 0 177 -147z"/>
                                <polygon class="fil96" points="3872,4670 4068,4600 4265,4253 4064,4361 "/>
                                <polygon class="fil97" points="3135,3432 3339,3324 3729,3559 3544,3671 "/>
                                <path class="fil98" d="M3305 4188c0,0 -19,120 193,262 212,143 482,335 640,544 158,212 262,532 35,814 0,0 -69,89 -189,127 -120,39 -131,-440 -131,-440l-170 -497 -455 -332 -201 -282 -39 -262 116 -127c0,0 224,-8 235,4 15,19 -35,189 -35,189z"/>
                                <path class="fil99" d="M2730 4836l189 -120c0,0 551,582 775,702l-66 212 -602 -289 -231 -293 -66 -212z"/>
                                <path class="fil100" d="M3536 6158l201 -127 -23 -193 -212 -58c0,0 -85,81 -81,93 4,12 35,96 35,96l81 189z"/>
                                <path class="fil101" d="M3872 4670l193 -308c0,0 -293,-305 -521,-459l0 -235 -409 -235 0 274c0,0 -455,-185 -517,139 -62,324 100,636 243,767 143,131 262,278 486,428 224,150 339,320 282,401 0,0 -177,189 -899,-605l-158 320c0,0 278,355 575,528l0 266 389 208 -12 -266c0,0 332,177 490,19 0,0 274,-189 8,-671 -85,-150 -197,-282 -328,-389l-370 -305c0,0 -193,-104 -231,-289 0,0 0,-154 174,-89 174,66 548,440 605,501z"/>
                            </g>
                            <polyline class="fil56" points="8102,12039 9270,12718 10042,12270 10813,12718 10427,12938 11584,13609 12741,12938 "/>
                            <polyline class="fil56 str2" points="8102,12039 9270,12718 10042,12270 10813,12718 10427,12938 11584,13609 12741,12938 "/>
                            <path class="fil56" d="M7824 12047l0 0c81,46 181,46 266,0l0 0c66,-39 66,-135 0,-174l0 0c-81,-46 -181,-46 -266,0l0 0c-66,42 -66,135 0,174z"/>
                            <polyline class="fil56" points="17800,9907 19343,9016 18186,8349 19343,7682 15074,5217 "/>
                            <polyline class="fil56 str2" points="17800,9907 19343,9016 18186,8349 19343,7682 15074,5217 "/>
                            <path class="fil56" d="M17519 10076l0 0c81,46 181,46 266,0l0 0c66,-39 66,-135 0,-174l0 0c-81,-46 -181,-46 -266,0l0 0c-66,39 -66,135 0,174z"/>
                            <path class="fil56" d="M6906 7065l0 0c81,46 181,46 266,0l0 0c66,-39 66,-135 0,-170l0 0c-81,-46 -181,-46 -266,0l0 0c-66,39 -66,135 0,170z"/>
                            <polyline class="fil56" points="8738,5121 7581,5792 8383,6220 7196,6903 "/>
                            <polyline class="fil56 str2" points="8738,5121 7581,5792 8383,6220 7196,6903 "/>
                            <polyline class="fil56" points="7986,4689 9124,5345 10666,4454 12209,5345 12602,5117 "/>
                            <polyline class="fil56 str2" points="7986,4689 9124,5345 10666,4454 12209,5345 12602,5117 "/>
                            <path class="fil56" d="M7712 4693l0 0c81,46 181,46 266,0l0 0c66,-39 66,-135 0,-170l0 0c-81,-46 -181,-46 -266,0l0 0c-66,35 -66,131 0,170z"/>
                            <path class="fil102" d="M1330 8310c-23,-31 -316,-204 -382,-239 0,0 -147,-100 -228,-89 -12,4 -27,15 -39,35 -50,359 4,451 4,451 8,85 -35,393 -62,490 -27,96 139,177 139,177l62 -220c0,0 8,112 8,262 27,4 220,139 316,135 35,0 66,-15 89,-31 -27,-143 -69,-478 -8,-814 8,-8 -46,-143 100,-158z"/>
                            <path class="fil103" d="M1238 8461l96 -154c-150,19 -96,154 -96,154z"/>
                            <polygon class="fil54" points="1284,8472 1330,8310 1238,8461 1242,8488 1269,8491 "/>
                            <g>
                                <g>
                                    <path class="fil81" d="M1967 8464c0,0 39,-27 42,-42 4,-15 0,-54 19,-66 19,-12 23,-4 19,12 -4,15 -4,27 -4,27 0,0 23,-15 35,-35 12,-19 19,-31 35,-23 15,8 62,46 31,104 -31,58 -89,104 -89,104l-89 -81z"/>
                                    <path class="fil53" d="M2032 8480c-23,-23 -73,-15 -73,-15l-42 35 81 116c0,0 39,-39 54,-54 0,-15 4,-58 -19,-81z"/>
                                    <path class="fil104" d="M1998 8545c-15,-39 -62,-50 -89,-46 -27,4 -247,181 -270,185 -23,4 -282,-343 -308,-374 -147,19 -96,154 -96,154 19,35 46,66 46,66 0,0 258,332 328,347 69,19 370,-243 382,-255 12,-12 19,-42 8,-77z"/>
                                </g>
                                <rect class="fil56" x="324" y="7408" width="1847" height="1847"/>
                            </g>
                            <g>
                                <g>
                                    <path class="fil105" d="M5900 9394l0 0 0 -224 -150 0c-316,-177 -698,-181 -1018,-8 -4,0 -8,4 -12,8l-112 0 0 224 0 0c8,73 46,139 116,181l19 12c316,181 706,189 1026,15l4 0c85,-46 123,-127 127,-208z"/>
                                    <path class="fil106" d="M4728 9363l19 12c316,181 706,189 1026,15l4 0c162,-89 166,-320 8,-413l-15 -8c-320,-185 -710,-193 -1033,-15l0 0c-166,85 -170,316 -8,409z"/>
                                </g>
                                <polygon class="fil107" points="5286,9087 5535,9082 5634,9154 5414,9153 5457,9186 5389,9219 5299,9153 5270,9153 5265,9165 5359,9234 5290,9268 5240,9231 5188,9366 5089,9293 5155,9152 5001,9227 4926,9172 5067,9103 4990,9046 5059,9012 5135,9069 5165,9054 5089,8997 5157,8964 5234,9020 5365,8956 5440,9011 "/>
                            </g>
                            <g>
                                <g>
                                    <path class="fil108" d="M7292 10173l0 0 0 -224 -150 0c-316,-177 -698,-181 -1018,-8 -4,0 -8,4 -12,8l-108 0 0 224 0 0c8,73 46,139 116,181l19 12c316,181 706,189 1026,15l4 0c81,-46 123,-127 123,-208z"/>
                                    <path class="fil109" d="M6120 10142l19 12c316,181 706,189 1026,15l4 0c162,-89 166,-320 8,-413l-15 -12c-320,-185 -710,-193 -1033,-15l0 0c-166,89 -166,320 -8,413z"/>
                                </g>
                                <polygon class="fil110" points="6662,9878 6912,9872 7010,9945 6790,9944 6834,9976 6765,10010 6675,9944 6646,9944 6641,9956 6735,10025 6667,10059 6616,10021 6564,10156 6465,10083 6531,9942 6377,10018 6302,9963 6443,9893 6366,9837 6435,9803 6512,9860 6542,9845 6465,9788 6533,9754 6610,9811 6741,9746 6816,9802 "/>
                            </g>
                            <polygon class="fil111" points="9736,10030 10075,10023 10208,10122 9909,10121 9969,10165 9876,10210 9754,10120 9715,10120 9708,10137 9835,10231 9742,10276 9674,10226 9603,10409 9469,10310 9558,10118 9350,10221 9248,10146 9439,10052 9335,9975 9428,9929 9532,10006 9573,9986 9469,9909 9562,9863 9666,9940 9844,9853 9945,9928 "/>
                            <polygon class="fil112" points="14157,6526 14495,6519 14629,6618 14330,6616 14390,6660 14297,6706 14175,6616 14136,6616 14129,6632 14256,6726 14163,6772 14095,6721 14024,6904 13890,6805 13979,6614 13771,6717 13669,6642 13860,6548 13756,6471 13849,6425 13953,6502 13994,6482 13890,6405 13983,6359 14087,6436 14265,6348 14366,6423 "/>
                            <g>
                                <polygon class="fil56" points="10150,5652 10445,5564 10951,5175 10656,5262 "/>
                                <polygon class="fil113" points="10656,5262 10951,5175 11472,5446 11177,5534 "/>
                                <polygon class="fil114" points="11177,5534 11472,5446 11002,5758 10707,5846 "/>
                                <polygon class="fil115" points="10707,5846 11002,5758 11234,5879 10939,5967 "/>
                                <polygon class="fil116" points="10150,6210 10445,6122 10445,6775 10150,6862 "/>
                                <polygon class="fil117" points="10462,6009 10757,5921 10695,5962 10399,6050 "/>
                                <polygon class="fil56" points="10150,6862 10445,6775 10048,6568 9753,6656 "/>
                                <polygon class="fil56" points="10617,7106 10913,7018 10445,6122 10150,6210 "/>
                                <polygon class="fil118" points="10442,6127 10738,6039 11234,6298 10939,6385 "/>
                                <polygon class="fil56" points="10939,6675 11234,6588 10966,6448 10670,6536 "/>
                                <polygon class="fil119" points="10670,6536 10966,6448 11436,7291 11140,7378 "/>
                                <polygon class="fil120" points="10939,6385 11234,6298 11234,6588 10939,6675 "/>
                                <polygon class="fil121" points="10399,6050 10695,5962 10738,6039 10442,6127 "/>
                                <polygon class="fil56" points="10939,6257 11234,6170 10757,5921 10462,6009 "/>
                                <polygon class="fil122" points="10939,5967 11234,5879 11234,6170 10939,6257 "/>
                                <polygon class="fil56" points="9753,6656 10048,6568 10048,5970 9753,6058 "/>
                                <polygon class="fil56" points="9753,6058 10048,5970 9642,5758 9346,5846 "/>
                                <polygon class="fil56" points="9346,5846 9642,5758 9642,5468 9346,5556 "/>
                                <polygon class="fil123" points="9346,5556 9642,5468 10048,5680 9753,5767 "/>
                                <polygon class="fil56" points="9753,5767 10048,5680 10048,5552 9753,5639 "/>
                                <polygon class="fil56" points="9753,5639 10048,5552 9642,5340 9346,5427 "/>
                                <polygon class="fil56" points="9346,5427 9642,5340 9642,5050 9346,5137 "/>
                                <polygon class="fil124" points="9346,5137 9642,5050 10048,5262 9753,5349 "/>
                                <polygon class="fil56" points="9753,5349 10048,5262 10048,4704 9753,4792 "/>
                                <polygon class="fil125" points="9753,4792 10048,4704 10445,4911 10150,4999 "/>
                                <polygon class="fil126" points="10150,4999 10445,4911 10445,5564 10150,5652 "/>
                                <polygon class="fil127" points="10150,5652 10656,5262 11177,5534 10707,5846 10939,5967 10939,6257 10462,6009 10399,6050 10442,6127 10939,6385 10939,6675 10670,6536 11140,7378 10617,7106 10150,6210 10150,6862 9753,6656 9753,6058 9346,5846 9346,5556 9753,5767 9753,5639 9346,5427 9346,5137 9753,5349 9753,4792 10150,4999 "/>
                            </g>
                        </g>
</svg>

                </div>
            </div>
        </div>
        <!--Container end-->
    </section>
    <!-- Hero Section End  -->
    <!--Landing Box Start -->
    <section>
        <div class="ico-status-main">
            <div class="container">
                <div class="ico-box row">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <!--Countdown Start-->
                        <div class="countdown-main d-flex justify-content-between flex-column">
                            <h5>ICO Ends in</h5>
                            <div id="timer" class="d-flex justify-content-start align-items-center">
                                <div class="c-time" id="days"></div>
                                <div class="c-time" id="hours"></div>
                                <div class="c-time" id="minutes"></div>
                                <div class="c-time" id="seconds"></div>
                            </div>
                        </div>
                        <!--Countdown End-->
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ico-info">
                        <div class="ico-status d-flex align-items-center">
                            <h4>$25365</h4>
                            <span>Raised</span>
                        </div>
                        <!--Progressbar start-->
                        <div class="ico-status-bar">
                            <span data-progress="8%"></span>
                        </div>
                        <!--Progressbar End-->
                        <div class="ico-status d-flex align-items-center">
                            <h4>19610</h4>
                            <span>coin Sold</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="ico-buttons">
                            <a class="btn-buy" href="{{ url('join') }}">Buy coins</a>
                            <a class="btn-white" href="{{ asset('Kwatcoin Whitepaper.pdf') }}" download>Whitepapers</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--Container End-->
        </div>
    </section>
    <!-- Landing Box End -->
    <!--About Us Area Start -->
    <section id="about">
        <!--About Main Container Start -->
        <div class="about-main-container">
            <div class="container">
                <div class="about-inner row about-reverse">
                    <!--About Left Start -->
                    <div class="about-left col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="ab-left-img">
                            <img src="{{ asset('default/images/about/about-img1.svg') }}" alt="About Image"/>
                        </div>
                    </div>
                    <!--About Left End -->
                    <!--About Right Start -->
                    <div class="about-right col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="about-right-inner">
                            <div class="about-description">
                                <!--Title-->
                                <div class="about-sec-heading">
                                    <div class="sec-title d-flex align-items-center">
                                        <div class="sec-line"></div>
                                        <div class="sec-shape">
                                            <img src="{{ asset('default/images/about/abt-icon.png') }}"
                                                 alt="About Icon"/>
                                        </div>
                                        <h2 class="title">About</h2>
                                    </div>
                                    <h3 class="sub-title">Why is Kwatcoin different</h3>
                                </div>
                                <p>
                                    In an era where most crypto currencies are either having problems or loosing value, yet there is a global demand for a more self sustainable crypto currency so as to aid buying and selling with other online based transactions.
                                    The kwatcoin proves itself as a more sustainable alternative for all.
                                    It's mission is to serve as a more valued medium of transaction thereby accurately meeting the desired needs of the global populace.
                                    The kwatcoin believes that it's users should be able to achieve a more standard value for their transactions in general, as it can be used in almost every major transactions and platforms around the world
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--About Right End -->
                </div>
                <div class="about-inner row">
                    <!--About Left Start -->
                    <div class="about-left col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="ab-left-img">
                            <img src="{{ asset('default/images/about/about-img2.svg') }}" alt="About Image"/>
                        </div>
                    </div>
                    <!--About Left End -->
                    <!--About Right Start -->
                    <div class="about-right col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="about-right-inner">
                            <div class="about-description">
                                <p>
                                    The kwatcoin is a blockchain service platform which has evolved within a space of time and has attracted a wide range of popularity and consistency
                                    It's a direct means of payment between an individual and others  without involving any financial institution whatsoever. It is an innovative network and a new kind of money designed to solve volatility problems and much more.
                                    It provides a peer to peer version of peer to peer  electric transactions and also proposes a peer to peer solution to double spending.

                                    The kwatcoin is currently running and enjoying a consistent successful run.

                                    The kwatcoin will be officially launched in the 1st of December 2019, which is in the last quarter of 2019
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--About Right End -->
                </div>
            </div>
            <!-- Container End -->
        </div>
        <!--About Main Container End -->
    </section>
    <!-- About Us Area End -->
    <!-- Feature Area Start-->
    <section id="feature">
        <!-- Feature Main Container Start-->
        <div class="feature-main-container">
            <div class="container">
                <!--title-->
                <div class="row section-heading d-flex align-items-center">
                    <div class="sec-title d-flex align-items-center col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="sec-line"></div>
                        <div class="sec-shape">
                            <img src="{{ asset('default/images/feature/feature-icon.png') }}" alt="Feature Icon"/>
                        </div>
                        <h2 class="title">Features</h2>
                    </div>
                    <h3 class="sub-title col-lg-6 col-md-12 col-sm-12 col-xs-12">Kwatcoin is the new kind of money and its the easy, fast and fun way of making payment or sending money to friends and businesses</h3>
                </div>
                <!--Tab Content Start-->
                <div class="feature-tabs text-center">
                    <!--Tab Links Start-->
                    <ul class="nav nav-tabs row" id="nav-tab" role="tablist">
                        <li class="nav-item col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <a
                                class="nav-link active"
                                id="nav-home-tab"
                                data-toggle="tab"
                                href="#nav-home"
                                role="tab"
                                aria-controls="nav-home"
                                aria-selected="true"
                            >
                                <div class="feature-tab-img">
                                    <svg
                                        version="1.1"
                                        id="Layer_2-tab1"
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px"
                                        y="0px"
                                        viewBox="0 0 129.58 109.2"
                                        style="enable-background:new 0 0 129.58 109.2;"
                                        xml:space="preserve"
                                    >
                      <style type="text/css">
                          .st0-tab1 {
                              fill: url(#SVGID_1_-tab1);
                          }

                          .st1-tab1 {
                              fill: url(#SVGID_2_-tab1);
                          }

                          .st2-tab1 {
                              opacity: 0.95;
                              fill: none;
                              stroke: #171a22;
                              stroke-width: 15;
                              stroke-miterlimit: 10;
                          }
                      </style>
                                        <g>
                                            <linearGradient
                                                id="SVGID_1_-tab1"
                                                gradientUnits="userSpaceOnUse"
                                                x1="69.607"
                                                y1="51.8203"
                                                x2="130.2348"
                                                y2="16.3974"
                                            >
                                                <stop offset="0" style="stop-color:#6D66D7"/>
                                                <stop offset="1" style="stop-color:#C28CF7;stop-opacity:0"/>
                                            </linearGradient>
                                            <polygon
                                                class="st0-tab1 cls-circle1"
                                                points="70.76,16.98 70.76,50.94 100.17,67.93 129.58,50.94 129.58,16.98 100.17,0 	"
                                            />
                                            <linearGradient
                                                id="SVGID_2_-tab1"
                                                gradientUnits="userSpaceOnUse"
                                                x1="-1.1517"
                                                y1="93.0962"
                                                x2="59.4761"
                                                y2="57.6733"
                                            >
                                                <stop offset="0" style="stop-color:#6D66D7"/>
                                                <stop offset="1" style="stop-color:#C28CF7;stop-opacity:0"/>
                                            </linearGradient>
                                            <polygon
                                                class="st1-tab1 cls-circle2"
                                                points="0,58.26 0,92.22 29.41,109.2 58.82,92.22 58.82,58.26 29.41,41.28 	"
                                            />
                                            <polygon
                                                class="st2-tab1 cls-circle3"
                                                points="28.45,34.48 28.45,75.89 64.31,96.6 100.17,75.89 100.17,34.48 64.31,13.77 	"
                                            />
                                        </g>
                    </svg>
                                </div>
                                <span class="text-capitalize">Mobile payments made easy</span>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <a
                                class="nav-item nav-link"
                                id="nav-profile-tab"
                                data-toggle="tab"
                                href="#nav-profile"
                                role="tab"
                                aria-controls="nav-profile"
                                aria-selected="false"
                            >
                                <div class="feature-tab-img">
                                    <svg
                                        version="1.1"
                                        id="Layer_2-tab2"
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px"
                                        y="0px"
                                        viewBox="0 0 108.56 108.31"
                                        style="enable-background:new 0 0 108.56 108.31;"
                                        xml:space="preserve"
                                    >
                      <style type="text/css">
                          .st0-tab2 {
                              fill: url(#SVGID_1_-tab2);
                          }

                          .st1-tab2 {
                              fill: url(#SVGID_2_-tab2);
                          }

                          .st2-tab2 {
                              opacity: 0.95;
                              fill: none;
                              stroke: #171a22;
                              stroke-width: 15;
                              stroke-miterlimit: 10;
                          }
                      </style>
                                        <g>
                                            <linearGradient
                                                id="SVGID_1_-tab2"
                                                gradientUnits="userSpaceOnUse"
                                                x1="63.4722"
                                                y1="85.7702"
                                                x2="108.5593"
                                                y2="85.7702"
                                            >
                                                <stop offset="0" style="stop-color:#6D66D7"/>
                                                <stop offset="1" style="stop-color:#C28CF7;stop-opacity:0"/>
                                            </linearGradient>
                                            <rect
                                                x="63.47"
                                                y="63.23"
                                                class="st0-tab2 rect1"
                                                width="45.09"
                                                height="45.09"
                                            />
                                            <linearGradient
                                                id="SVGID_2_-tab2"
                                                gradientUnits="userSpaceOnUse"
                                                x1="32.258"
                                                y1="56.8876"
                                                x2="77.3451"
                                                y2="56.8876"
                                            >
                                                <stop offset="0" style="stop-color:#6D66D7"/>
                                                <stop offset="1" style="stop-color:#C28CF7;stop-opacity:0"/>
                                            </linearGradient>
                                            <rect
                                                x="32.26"
                                                y="34.34"
                                                class="st1-tab2 rect2"
                                                width="45.09"
                                                height="45.09"
                                            />
                                            <rect
                                                x="7.48"
                                                y="7.82"
                                                class="st2-tab2 rect3"
                                                width="45.09"
                                                height="45.09"
                                            />
                                        </g>
                    </svg>
                                </div>
                                <span class="text-capitalize">Security and control over your money</span>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <a
                                class="nav-item nav-link"
                                id="nav-contact-tab"
                                data-toggle="tab"
                                href="#nav-contact"
                                role="tab"
                                aria-controls="nav-contact"
                                aria-selected="false"
                            >
                                <div class="feature-tab-img">
                                    <svg
                                        version="1.1"
                                        id="Layer_2-tab3"
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px"
                                        y="0px"
                                        viewBox="0 0 129.11 116.42"
                                        style="enable-background:new 0 0 129.11 116.42;"
                                        xml:space="preserve"
                                    >
                      <style type="text/css">
                          .st0-tab3 {
                              fill: url(#SVGID_1_-tab3);
                          }

                          .st1-tab3 {
                              opacity: 0.95;
                              fill: none;
                              stroke: #171a22;
                              stroke-width: 15;
                              stroke-miterlimit: 10;
                          }
                      </style>
                                        <g>
                                            <linearGradient
                                                id="SVGID_1_-tab3"
                                                gradientUnits="userSpaceOnUse"
                                                x1="8.2222"
                                                y1="90.6888"
                                                x2="129.115"
                                                y2="90.6888"
                                            >
                                                <stop offset="0" style="stop-color:#6D66D7"/>
                                                <stop offset="1" style="stop-color:#C28CF7;stop-opacity:0"/>
                                            </linearGradient>
                                            <rect x="8.22" y="64.95" class="st0-tab3" width="120.89" height="51.47"/>
                                            <path
                                                class="st1-tab3 cls-pen"
                                                d="M79.18,43.91L39.17,83.92L8.22,85.28l1.36-30.95L49.6,14.33c8.17-8.17,21.41-8.17,29.58,0l0,0
                                       C87.35,22.5,87.35,35.74,79.18,43.91z"
                                            />
                                        </g>
                    </svg>
                                </div>
                                <span class="text-capitalize">Protect your identity</span>
                            </a>
                        </li>
                        <li class="nav-item col-lg-3 col-md-6 col-sm-12 col-xs-12">
                            <a
                                class="nav-item nav-link"
                                id="nav-card-tab"
                                data-toggle="tab"
                                href="#nav-card"
                                role="tab"
                                aria-controls="nav-card"
                                aria-selected="false"
                            >
                                <div class="feature-tab-img">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 58 58" style="enable-background:new 0 0 58 58;"
                                         xml:space="preserve">
<g id="XMLID_7_">
    <path id="XMLID_130_" style="fill:#6BBEA1;" d="M54.783,50H3.217C1.439,50,0,48.56,0,46.783V11.217C0,9.44,1.439,8,3.217,8h51.566
		C56.56,8,58,9.44,58,11.217v35.566C58,48.56,56.56,50,54.783,50"/>
    <rect id="XMLID_129_" x="5" y="37" style="fill:#498679;" width="9" height="6"/>
    <rect id="XMLID_128_" x="18" y="37" style="fill:#498679;" width="9" height="6"/>
    <rect id="XMLID_127_" x="31" y="37" style="fill:#498679;" width="9" height="6"/>
    <rect id="XMLID_126_" x="44" y="37" style="fill:#498679;" width="9" height="6"/>
    <path id="XMLID_125_" style="fill:#F0C41B;" d="M19.745,26H6.255C5.563,26,5,25.438,5,24.745v-9.49C5,14.562,5.563,14,6.255,14
		h13.49C20.438,14,21,14.562,21,15.255v9.49C21,25.438,20.438,26,19.745,26"/>
    <path id="XMLID_122_" style="fill:#F3D55C;" d="M19.745,27H6.255C5.012,27,4,25.988,4,24.745v-9.49C4,14.012,5.012,13,6.255,13
		h13.49C20.988,13,22,14.012,22,15.255v9.49C22,25.988,20.988,27,19.745,27 M6.255,15C6.113,15,6,15.114,6,15.255v9.49
		C6,24.886,6.113,25,6.255,25h13.49C19.886,25,20,24.886,20,24.745v-9.49C20,15.114,19.886,15,19.745,15H6.255z"/>
    <path id="XMLID_121_" style="fill:#F0C41B;" d="M21,15.255C21,14.561,20.438,14,19.745,14H15h-2.745C11.561,14,11,14.561,11,15.255
		V18v6.745C11,25.438,11.561,26,12.255,26h1.49C14.438,26,15,25.438,15,24.745v-5.49C15,18.562,15.561,18,16.255,18h3.49
		C20.438,18,21,17.438,21,16.745V15.255z"/>
    <path id="XMLID_118_" style="fill:#F3D55C;" d="M13.745,27h-1.49C11.012,27,10,25.988,10,24.745v-9.49
		C10,14.012,11.012,13,12.255,13h7.49C20.988,13,22,14.012,22,15.255v1.49C22,17.988,20.988,19,19.745,19h-3.49
		C16.113,19,16,19.114,16,19.255v5.49C16,25.988,14.988,27,13.745,27 M12.255,15C12.113,15,12,15.114,12,15.255v9.49
		C12,24.886,12.113,25,12.255,25h1.49C13.886,25,14,24.886,14,24.745v-5.49C14,18.012,15.012,17,16.255,17h3.49
		C19.886,17,20,16.886,20,16.745v-1.49C20,15.114,19.886,15,19.745,15H12.255z"/>
    <line id="XMLID_117_" style="fill:#F0C41B;" x1="15" y1="22" x2="21" y2="22"/>
    <path id="XMLID_116_" style="fill:#F3D55C;" d="M21,23h-6c-0.553,0-1-0.448-1-1s0.447-1,1-1h6c0.553,0,1,0.448,1,1S21.553,23,21,23
		"/>
    <line id="XMLID_115_" style="fill:#F0C41B;" x1="11" y1="18" x2="5" y2="18"/>
    <path id="XMLID_114_" style="fill:#F3D55C;"
          d="M11,19H5c-0.553,0-1-0.448-1-1s0.447-1,1-1h6c0.553,0,1,0.448,1,1S11.553,19,11,19"
    />
    <line id="XMLID_113_" style="fill:#F0C41B;" x1="11" y1="22" x2="5" y2="22"/>
    <path id="XMLID_112_" style="fill:#F3D55C;"
          d="M11,23H5c-0.553,0-1-0.448-1-1s0.447-1,1-1h6c0.553,0,1,0.448,1,1S11.553,23,11,23"
    />
    <path id="XMLID_111_" style="fill:#498679;" d="M40,13h-9c-0.553,0-1,0.448-1,1s0.447,1,1,1h9c0.553,0,1-0.448,1-1S40.553,13,40,13
		"/>
    <path id="XMLID_110_" style="fill:#498679;" d="M54,14c0-0.552-0.447-1-1-1h-9c-0.553,0-1,0.448-1,1s0.447,1,1,1h9
		C53.553,15,54,14.552,54,14"/>
    <path id="XMLID_109_" style="fill:#498679;" d="M32,18h-1c-0.553,0-1,0.448-1,1s0.447,1,1,1h1c0.553,0,1-0.448,1-1S32.553,18,32,18
		"/>
    <path id="XMLID_108_" style="fill:#498679;" d="M38,18h-2c-0.553,0-1,0.448-1,1s0.447,1,1,1h2c0.553,0,1-0.448,1-1S38.553,18,38,18
		"/>
    <path id="XMLID_107_" style="fill:#498679;" d="M43,18h-1c-0.553,0-1,0.448-1,1s0.447,1,1,1h1c0.553,0,1-0.448,1-1S43.553,18,43,18
		"/>
    <path id="XMLID_106_" style="fill:#498679;" d="M49,18h-2c-0.553,0-1,0.448-1,1s0.447,1,1,1h2c0.553,0,1-0.448,1-1S49.553,18,49,18
		"/>
    <path id="XMLID_105_" style="fill:#498679;" d="M52.29,18.29C52.109,18.48,52,18.74,52,19c0,0.26,0.109,0.52,0.29,0.71
		C52.479,19.89,52.729,20,53,20c0.26,0,0.519-0.11,0.71-0.29C53.89,19.52,54,19.26,54,19c0-0.26-0.11-0.52-0.29-0.71
		C53.33,17.92,52.66,17.92,52.29,18.29"/>
</g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
</svg>

                                </div>
                                <span class="text-capitalize">Fast payments</span>
                            </a>
                        </li>
                    </ul>
                    <!--Tab Links End-->
                    <!--Tab Detail Start-->
                    <div class="tab-content" id="nav-tabContent">
                        <div
                            class="tab-pane fade show active"
                            id="nav-home"
                            role="tabpanel"
                            aria-labelledby="nav-home-tab"
                        >
                            <div class="tab-detail">
                                <p>
                                    Kwatcoin when used on a mobile device allows you to pay with a simple two-step scan-and-pay. There's no need to sign up, swipe your card, type a PIN, or sign anything. All you need to receive kwatcoin payments is to display the QR code in your Kwatcoin wallet app and let the other party scan your mobile or QR code sticker. You can also your the kwatcoin verve card for other online platforms that don't accept kwatcoin.
                                </p>
                            </div>
                        </div>
                        <div
                            class="tab-pane fade"
                            id="nav-profile"
                            role="tabpanel"
                            aria-labelledby="nav-profile-tab"
                        >
                            <div class="tab-detail">
                                <p>
                                    Kwatcoin transactions are secured by military-grade technology called the crytonote. Nobody can take your money or make a payment on your behalf. So long as you take the required steps to protect your wallet, kwatcoin gives you control over your money and a strong level of protection against many types of fraud.
                                </p>
                            </div>
                        </div>
                        <div
                            class="tab-pane fade"
                            id="nav-contact"
                            role="tabpanel"
                            aria-labelledby="nav-contact-tab"
                        >
                            <div class="tab-detail">
                                <p>
                                    Kwatcoin  uses  ring  signatures,  ring  confidential  transactions,  and  stealth  addresses  to obfuscate  the  origins,  amounts,  and  destinations  of  all transactions.  Kwatcoin  provides  all  the benefits  of  a  decentralized  cryptocurrency,  without  any  of  the  typical  privacy  concessions.
                                    With Kwatcoin, there's no credit card number that malicious actors can collect in order to steal from you. In fact, You send a payment without revealing your identity, almost like with physical money. You should, however, take note that some effort can be required to protect your privacy.
                                </p>
                            </div>
                        </div>
                        <div
                            class="tab-pane fade"
                            id="nav-card"
                            role="tabpanel"
                            aria-labelledby="nav-card-tab"
                        >
                            <div class="tab-detail">
                                <p>
                                    Sending kwatcoins across borders is as easy and fun as sending them across the street. There are no banks to make you wait three business days, no extra fees for making a local or an international transfer, and no special limitations on the minimum or maximum amount you can send. With Kwatcoin you are truely borderless.
                                </p>

                            </div>
                        </div>
                    </div>
                    <!--Tab Detail End-->
                </div>
                <!--Tab Content End-->
            </div>
            <!--Container End-->
        </div>
    </section>
    <!-- Feature Area End-->

    <!--FAQ Area Start -->
    <section id="faq">
        <!--FAQ Main Container Start -->
        <div class="faq-main-container">
            <div class="container">
                <!--title-->
                <div class="row section-heading d-flex align-items-center">
                    <div class="sec-title d-flex align-items-center col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="sec-line"></div>
                        <div class="sec-shape">
                            <img src="{{ asset('default/images/faq/faq-icon.png') }}" alt="FAQ Icon"/>
                        </div>
                        <h2 class="title">faq</h2>
                    </div>

                </div>
                <!--Faq Start-->
                <div class="faq-r">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <!--Card Start-->
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingone"
                                data-toggle="collapse"
                                data-target="#collapseone"
                                aria-expanded="false"
                                aria-controls="collapseone"
                            >
                                <div class="faq-head mb-0">
                                    <h3>How meaningful is the kwatcoins utility </h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseone"
                                class="collapse"
                                aria-labelledby="headingone"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p>
                                        it's utility is at maximum capacity giving it's flexibility, durability, and anonymous standards
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Card End-->
                        <!--Card Start-->
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseThree"
                                aria-expanded="false"
                                aria-controls="collapseThree"
                            >
                                <div class="faq-head mb-0">
                                    <h3>What's the certainty of it's continuous existence in the next 10 years</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseThree"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p> kwatcoin has been designed to serve as a leader in the crypto market, as such it has been fitted with standard sustainable abilities</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseFour"
                                aria-expanded="false"
                                aria-controls="collapseFour"
                            >
                                <div class="faq-head mb-0">
                                    <h3>Does the kwatcoin go against financial laws e.g security laws</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseFour"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p>There is no law restricting it's total functionality giving the fact that the kwatcoin is totally decentralised and it's not dependent on any form of central authority to thrive</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseFive"
                                aria-expanded="false"
                                aria-controls="collapseFive"
                            >
                                <div class="faq-head mb-0">
                                    <h3>What's the possible best term duration for kwatcoins purchase</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseFive"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p>As long as the crypto market still exists and it's usage as a standard medium for online financial transactions</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseSix"
                                aria-expanded="false"
                                aria-controls="collapseSix"
                            >
                                <div class="faq-head mb-0">
                                    <h3>Is the kwatcoin regulated by any form of laws</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseSix"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p>It has no regulations, it operates on all forms of market policies</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseSeven"
                                aria-expanded="false"
                                aria-controls="collapseSeven"
                            >
                                <div class="faq-head mb-0">
                                    <h3>What's the present status of demand</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseSeven"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p>The existence of the kwatcoin is in tandem existence of the dollar, euro, pounds and other string currencies around the world.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseEight"
                                aria-expanded="false"
                                aria-controls="collapseEight"
                            >
                                <div class="faq-head mb-0">
                                    <h3>What's the present status of demand</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseEight"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p>The existence of the kwatcoin is in tandem existence of the dollar, euro, pounds and other string currencies around the world.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseNine"
                                aria-expanded="false"
                                aria-controls="collapseNine"
                            >
                                <div class="faq-head mb-0">
                                    <h3>what is the present demand status of the kwatcoin</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseNine"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p> It's demand can best be termed as unimaginable, giving the rush.

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div
                                class="card-header collapsed"
                                role="tablist"
                                id="headingThree"
                                data-toggle="collapse"
                                data-target="#collapseTen"
                                aria-expanded="false"
                                aria-controls="collapseTen"
                            >
                                <div class="faq-head mb-0">
                                    <h3>Why should I risk my money investing in kwatcoin</h3>
                                    <div class="faq-icon-main"></div>
                                </div>
                            </div>
                            <div
                                id="collapseTen"
                                class="collapse"
                                aria-labelledby="headingThree"
                                data-parent="#accordion"
                            >
                                <div class="card-body">
                                    <p> Investing in kwatcoin is nothing short of a most worthy experience, the gains is worth the risk
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--Card End-->

                    </div>
                </div>
                <!--Faq End-->
            </div>
            <!--Container End-->
        </div>
    </section>
    <!--Partner Area Start -->
    <section id="partner">
        <!--Partner Main Container Start -->
        <div class="partner-main-container">
            <div class="container">
                <!--title-->
                <div class="row section-heading d-flex align-items-center">
                    <div class="sec-title d-flex align-items-center col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <div class="sec-line"></div>
                        <div class="sec-shape">
                            <img src="{{ asset('default/images/partner/partner-icon.png') }}" alt="Partners Icon"/>
                        </div>
                        <h2 class="title">partners</h2>
                    </div>

                </div>
                <!--Partner Start-->
                <div class="our-partner">
                    <ul class="row partner-list">
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partners/1.jpeg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partners/2.jpeg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partners/3.jpeg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partners/4.jpeg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partners/5.jpeg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partners/6.jpeg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/partner3.jpg') }}" alt="Partner Image"/>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <img src="{{ asset('images/astria.svg') }}" alt="Partner Image"/>
                        </li>

                    </ul>
                </div>
                <!--Partners End-->
                <div class="footer-box-main" id="contact">
                    <!--Footer Box Start-->
                    <div class="footer-box">
                        <div class="footer-box-top d-flex justify-content-between flex-wrap">
                            <div class="footer-box-text">Connect on</div>
                            <ul class="foot-social d-flex align-items-center">

                                <li>
                                    <a href="https://facebook.com/@kwatcoin" target="_blank">
                                        <img src="{{ asset('default/images/facebook.png') }}" alt="Facebook Logo"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/@kwatcoin" target="_blank">
                                        <img src="{{ asset('default/images/twitter.png') }}" alt="Twitter Logo"/>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://instagram.com/@kwatcoin" target="_blank">
                                        <img src="{{ asset('default/images/instagram.png') }}" alt="Instagram Logo"/>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-box-bottom text-center">
                            <h5 class="footer-b-text">Still have questions? We’re happy to help.</h5>
                            <div class="footer-b-buttons">
                                <a class="btn-phone" href="tel:+015564489999">
                                    <img src="{{ asset('default/images/phone.png') }}" alt="Phone Icon"/>
                                    +2349015218911
                                </a>
                                <a class="btn-phone" href="tel:+015564489999">
                                    <img src="{{ asset('default/images/phone.png') }}" alt="Phone Icon"/>
                                    +2349039717689
                                </a>
                                <a class="btn-email" href="mailto:info@Kwatcoin.com">
                                    <img src="{{ asset('default/images/email.png') }}" alt="Email Icon"/>
                                    info@Kwatcoin.org
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--Footer Box End-->
                    <!--Footer Start-->
                    <div class="foot-info d-flex justify-content-between align-items-center flex-wrap">
                        <div class="foot-copyright">© {{ date('Y') }} Kwatcoin, All Right Reserved</div>
                        <ul class="foot-links d-flex flex-wrap">
                            <li>
                                <a href="#">Terms</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                    <!--Footer End-->
                </div>
            </div>
            <!--Container End-->
        </div>
    </section>
</section>
<script src="{{ asset('default/assets/js/highcharts.js') }}"></script>
<script src="{{ asset('default/assets/js/highcharts-3d.js') }}"></script>
<script src="{{ asset('assets/js/exporting.js') }}"></script>
<script src="{{ asset('default/assets/js/export-data.js') }}"></script>
<script src="{{ asset('default/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('default/assets/js/Popper.js') }}"></script>
<script src="{{ asset('default/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('default/assets/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('default/assets/js/main.js') }}"></script>
<script src="{{ asset('default/assets/js/slick.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>

<script>
    @if(\Illuminate\Support\Facades\Session::has('success'))
    new Noty({
        type: 'success',
        layout: 'topRight',
        text: "{{ \Illuminate\Support\Facades\Session::get('success') }}",
        theme: 'semanticui',
        timeout: 10000
    }).show();
    @elseif(\Illuminate\Support\Facades\Session::has('error'))
    new Noty({
        type: 'error',
        layout: 'topRight',
        text: "{{ \Illuminate\Support\Facades\Session::get('error') }}",
        theme: 'semanticui',
        timeout: 10000
    }).show();
    @elseif(\Illuminate\Support\Facades\Session::has('alert'))
    new Noty({
        type: 'alert',
        layout: 'topRight',
        text: "{{ \Illuminate\Support\Facades\Session::get('alert') }}",
        theme: 'semanticui',
        timeout: 10000
    }).show();
    @endif
</script>
</body>


</html>
